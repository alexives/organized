# frozen_string_literal: true
require 'gitlab'
require 'yaml'
require 'forwardable'
require 'semantic'

require_relative 'organized/version'
require_relative 'organized/issue'
require_relative 'organized/group_issues'
require_relative 'organized/epic'
require_relative 'organized/group_epics'
require_relative 'organized/mentions'
require_relative 'organized/member_issues'
require_relative 'organized/milestone'
require_relative 'organized/current_milestone'
require_relative 'organized/personal_issue'

module Organized
  class Error < StandardError; end
  def self.config
    config = YAML.load_file('config.yml')
  end

  def self.configure!
    Gitlab.endpoint = config['endpoint']
    Gitlab.private_token = config['private_token']
    nil
  end
  # Your code goes here...
end
