class MemberIssues
  OPENED = 'opened'
  attr_accessor :issues

  def initialize(gitlab, member)
    @issues = Gitlab.issues(nil, {assignee_username: member, scope: 'all', state: OPENED}).map do |issue|
      Issue.new(issue)
    end
  end
end
