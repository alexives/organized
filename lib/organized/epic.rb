
class Epic
  extend Forwardable
  attr_accessor :epic
  def initialize(epic)
    @epic = epic
  end

  def_delegators :@epic, :title, :web_url, :description
end
