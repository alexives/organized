class Milestone
  attr_accessor :milestone, :retrospective, :planning, :epics

  def initialize(config, milestone)
    @milestone = milestone
    search = {search: milestone.title, in: 'title', state: 'opened'}

    @planning = Issue.new(Gitlab.issues(project = config['planning_project'], options=search).first)
    @retrospective = Issue.new(Gitlab.issues(project = config['retro_project'], options=search).first)

    epic_list = has_open_planning? ? @planning.description.scan(/https.*gitlab.*epics.*\d/) : []
    @epics = epic_list.map do |epic_url|
      project = epic_url.gsub(/https.*groups\//, '').gsub(/\/-.*/,'').gsub('/', '%2F')
      epic_id = epic_url.gsub(/.*epics\//,'')
      Epic.new(Gitlab.epic(project, epic_id))
    end
  end

  def has_open_planning?
    !@planning.nil?
  end

  def has_open_retro?
    !@retrospective.nil?
  end

  def current?
    Date.parse(milestone.start_date) < Date.today && Date.parse(milestone.due_date) > Date.today
  end
end
