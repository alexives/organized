class Issue
  extend Forwardable
  attr_accessor :response

  def_delegators :@response, :iid, :project_id, :title, :web_url, :description, :nil?

  def initialize(response)
    self.response = response
  end

  def update!
    self.response = Gitlab.issue(project_id, iid)
  end

  def remove_label(label)
    labels.delete(label)
    Gitlab.edit_issue(project_id, iid, {labels: labels})
  end

  def add_label(label)
    labels << label
    Gitlab.edit_issue(project_id, iid, {labels: labels})
  end

  def update_description(new_description)
    Gitlab.edit_issue(project_id, iid, {description: new_description})
  end
end
