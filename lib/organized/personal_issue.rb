class PersonalIssue
  attr_accessor :issue, :milestone, :epics, :twenty_issues_no_epic

  def initialize(config)
    @issue = Issue.new(Gitlab.issue(config['personal_issue']['project_id'], config['personal_issue']['issue_iid']))
    @milestone = CurrentMilestone.new(config)
    @epics = GroupEpics.new(config['group_id'], config['label'])
    @twenty_issues_no_epic = Gitlab.group_issues(config['group_id'], {labels: [config['label']], state: 'opened', per_page: 20, epic_id: 'None'}).map do |issue|
      Issue.new(issue)
    end
  end

  def update!
    planning_issue = milestone.planning(:has_open_planning?).web_url
    retro_issue = milestone.retrospective(:has_open_retro?).web_url
    current_description = issue.description.split('<!-- END GENERATED -->').last
    epics_in_milestone = milestone.epics.map do |epic|
      "* #{epic.web_url}+"
    end.join("\n")
    group_epics = epics.map do |epic|
      "- [ ] #{epic.web_url}+"
    end.join("\n")
    no_epic_issues_list = twenty_issues_no_epic.map do |issue|
      "- [ ] #{issue.web_url}+"
    end.join("\n")
    

    new_description = <<EOF
### [#{milestone.milestone(:has_open_planning?).title} Planning Issue](#{planning_issue})
### [#{milestone.milestone(:has_open_retro?).title} Retro Issue](#{retro_issue})

### Epics in Milestone
#{epics_in_milestone}

<p>
<details>
<summary>Database Team Epics</summary>

#{group_epics}

</details>
</p>

<p>
<details>
<summary>20 Database Team Issues without an Epic</summary>

#{no_epic_issues_list}

</details>
</p>

<!-- END GENERATED -->
#{current_description}
EOF
    # puts new_description
    issue.update_description(new_description)
    puts "Updated issue with #{milestone.milestone(:has_open_planning?).title} links"
  end
end
