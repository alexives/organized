class CurrentMilestone
  attr_accessor :milestones
  def initialize(config)
    @milestones = Gitlab.group_milestones('gitlab-org', {state: 'active'}).select{|m| !m.expired}.sort { |a, b| DateTime.parse(a.start_date) <=> DateTime.parse(b.start_date) }.map do |milestone|
      Milestone.new(config, milestone)
    end
  end

  def milestone(method = :has_open_planning?)
    @milestones.find(&method).milestone
  end

  def retrospective(method = :has_open_planning?)
    @milestones.find(&method).retrospective
  end

  def planning(method = :has_open_planning?)
    @milestones.find(&method).planning
  end

  def epics(method = :has_open_planning?)
    @milestones.find(&method).epics
  end
end
