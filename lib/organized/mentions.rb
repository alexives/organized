class Mentions
  attr_accessor :discussions
  def initialize()
    @discussions = discussions_from_todos
  end

  def discussions_for_todo(todo)
    case todo.target_type
    when 'Issue'
      Gitlab.get(
        "/projects/#{Gitlab.url_encode todo.project.path_with_namespace}/issues/#{todo.target.iid}/discussions",
        query: {per_page: 100}
      )
    when 'Epic'
      Gitlab.get(
        "/groups/#{Gitlab.url_encode todo.group.full_path}/epics/#{todo.target.id}/discussions",
        query: {per_page: 100}
      )
    when 'MergeRequest'
      Gitlab.get(
        "/projects/#{Gitlab.url_encode todo.project.path_with_namespace}/merge_requests/#{todo.target.iid}/discussions",
        query: {per_page: 100}
      )
    else
      puts todo.target_url
      []
    end
  end

  def discussions_from_todos
    Gitlab.todos(state: 'done', action: 'mentioned', per_page: 100).map do |todo|
      discussions_for_todo(todo).reject do |discussion|
        discussion.notes.select do |note|
          note.id == todo.target_url.sub(/.*note_/, '').to_i
        end.empty?
      end
    end.reject(&:empty?)
  end
end

