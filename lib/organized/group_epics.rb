class GroupEpics
  attr_accessor :issues

  def initialize(group, label, options={})
    self.issues = Gitlab.epics(group, options.merge({labels: [label]})).auto_paginate.map do |issue|
      Epic.new(issue)
    end
  end

  private

  def respond_to_missing?(name, include_private = false)
    issues.respond_to?(name, include_private)
  end

  def method_missing(method, *args, &block)
    issues.public_send(method, *args, &block)
  end
end
