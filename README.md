# Organized

I would like an issue created every day with:
- List of issues currently assigned to me
- List of issues in hidden list assigned to members of the team with their last update date
- Current team retrospective
- Epics in current team planning issue
- This weeks un-responded to group mentions
- This weeks un-responded to personal mentions
- Note closed issues to ping for retro items
- Generate summary for Engineering Allocation
- Option to auto close issues at the end of the day
- Merge request stats
